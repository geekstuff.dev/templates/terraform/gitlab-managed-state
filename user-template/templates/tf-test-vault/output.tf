output "abc" {
  value = var.abc
}

output "commit_sha" {
  value = var.commit_sha
}
