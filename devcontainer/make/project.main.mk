#!make

# hide entering/leaving directory messages
MAKEFLAGS += --no-print-directory

all: info

include ${DC_LIB_MAKE}/project.info.mk
include ${DC_LIB_MAKE}/terraform.mk
include ${DC_LIB_MAKE}/vault.mk
