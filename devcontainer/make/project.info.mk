#!make

.PHONY: info
info:
	@echo "Welcome to Terraform sub-folder project, in TF Devcontainer"
	@echo ""
	@echo "Things to try:"
	@echo "  terraform {init,validate,plan,apply}*  # Regular TF commands"
	@echo "  make tf-{init,validate,plan,apply}     # If using vault, these will prepend TF commands with VAULT_TOKEN=X"
	@echo "  make vault-*                           # If VAULT_* vars are set"
