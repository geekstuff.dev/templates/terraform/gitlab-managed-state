#!make

.PHONY: info
info:
	@echo "Welcome to TF Devcontainer"
	@echo ""
	@echo "Things to try:"
	@echo "  make check.vars            # After direnv kicks in, this command reports missing vars config"
	@echo "  make vault-login           # If VAULT_* vars are set, logs you into vault using oidc"
	@echo "  make template-diff         # Show difference between with container template"
	@echo "  cp -r templates/tf-test .  # Copy one of the provided templates to into project root"
	@echo "                             # ready to deploy and iterate on"
	@echo ""
	@echo "Devcontainer features:"
	@echo "  - Use TAB to auto-complete or discover make commands"
	@echo "  - Within a devcontainer, you can use VSCode to browse other files in the container:"
	@echo "    'code \$$DC_LIB'"
